#include <iostream>

#include "CSCMatrix.h"

CSCMatrix CSCMatrix::transposeMatrix() {
	int n = columns.size() - 1;
	std::vector<int> counts = std::vector<int>(n, 0);
	CSCMatrix transposedMatrix = CSCMatrix(values.size());

	for (int i = 0; i < n; i++) {
		for (int j = columns[i]; j < columns[i + 1]; j++) {
			int k = rows[j];
			counts[k]++;
		}
	}

	transposedMatrix.getColumns().push_back(0);
	for (int j = 0; j < n; j++) {
		transposedMatrix.getColumns().push_back(transposedMatrix.getColumns()[j] + counts[j]);
		counts[j] = 0;
	}

	for (int i = 0; i < n; i++) {
		for (int j = columns[i]; j < columns[i + 1]; j++) {
			int k = rows[j];
			int index = transposedMatrix.getColumns()[k] + counts[k];
			transposedMatrix.getRows()[index] = i;
			transposedMatrix.getValues()[index] = values[j];
			counts[k]++;
		}
	}

	return transposedMatrix;
}

void CSCMatrix::insertResults(Matrix& frontalMatrix, std::vector<int>& indeces) {
	for (int i = 0; i < indeces.size(); i++) {
		std::vector<int> insertedIndeces = std::vector<int>(frontalMatrix.getIndeces().begin() + i, frontalMatrix.getIndeces().end());
		std::vector<double> column = frontalMatrix.getValues()[i];
		std::vector<double> insertedValues = std::vector<double>(column.begin() + i, column.end());	
		int insertIndex = columns[indeces[i]];
		int counter = 0;
		for (int j = 0; j < insertedIndeces.size(); j++) {
			if (insertedValues[j] != 0) {
				rows.insert(rows.begin() + insertIndex + counter, insertedIndeces[j]);
				values.insert(values.begin() + insertIndex + counter, insertedValues[j]);
				counter++;
			}
		}

		for (int k = indeces[i] + 1; k < columns.size(); k++) {
			columns[k] += counter;
		}
	}
}

void CSCMatrix::printMatrix() {
	std::cout << "Rows:            ";
	for (int i = 0; i < rows.size(); i++) {
		std::cout << rows[i] << " ";
	}

	std::cout << std::endl << "Column pointers: ";
	for (int i = 0; i < columns.size(); i++) {
		std::cout << columns[i] << " ";
	}

	std::cout << std::endl << "Values:          ";
	for (int i = 0; i < values.size(); i++) {
		std::cout << values[i] << " ";
	}
	std::cout << std::endl;
}