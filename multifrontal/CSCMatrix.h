#pragma once

#include <vector>

#include "Matrix.h"

//represents sparse matrix in CSC format
class CSCMatrix {

private:
	std::vector<int> columns = std::vector<int>();
	std::vector<int> rows = std::vector<int>();
	std::vector<double> values = std::vector<double>();

public:
	CSCMatrix() {}

	CSCMatrix(int n) {
		columns = std::vector<int>();
		values = std::vector<double>(n);
		rows = std::vector<int>(n);
	}

	// returns transpose of current matrix
	CSCMatrix transposeMatrix();

	// inserts "eliminated" columns in results matrix (more at one time if given node is supernode)
	void insertResults(Matrix& frontalMatrix, std::vector<int>& indeces);
	void printMatrix();

	void setColumns(std::vector<int> columns) {
		this->columns = columns;
	}

	std::vector<int>& getColumns() { return columns; }
	std::vector<int>& getRows() { return rows; }
	std::vector<double>& getValues() { return values; }
};
