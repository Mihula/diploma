#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>

class DataCollector {

private:
	clock_t begin;
	clock_t end;
	double totalTime = 0;
	int originalNodesCount;
	int allSuperNodals;
	int superNodesCount;
	
	int roots;
	int leaves;
	std::vector<double> measurements = std::vector<double>();
	std::vector<std::string> names = std::vector<std::string>();

	void measure() {
		end = clock();
		double elapsed = double(end - begin) / CLOCKS_PER_SEC;
		std::cout << " (" << elapsed << " seconds)" << std::endl;
		totalTime += elapsed;
		measurements.push_back(elapsed);
	}

public:
	DataCollector() {}

	void start(std::string operation) {
		names.push_back(operation);
		std::cout << "-- " << operation << " --";
		begin = clock();
	}

	void split(std::string operation) {
		measure();
		start(operation);
	}

	void pause() {
		measure();
	}

	void stop() {
		std::cout << "-- total time: " << totalTime << " seconds --" << std::endl;
	}

	void setRootsAndLeaves(int roots, int leaves) {
		this->roots = roots;
		this->leaves = leaves;
	}

	void setNodes(int originalNodes, int superNodals, int superNodes) {
		this->originalNodesCount = originalNodes;
		this->allSuperNodals = superNodals;
		this->superNodesCount = superNodes;
	}

	void exportData(std::string fileName) {
		std::cout << "-- exporting data to " << fileName << " --" << std::endl;

		std::ofstream file;
		file.open(fileName, std::ios_base::app);
		file << "times: " << std::endl;
		for (int i = 0; i < measurements.size(); i++) {
			file << names[i] << " " << measurements[i] << " seconds" << std::endl;
		}
		file << "total time: " << totalTime << " seconds" << std::endl;
		file << "roots: " << roots << std::endl;
		file << "leaves: " << leaves << std::endl;
		file << "original nodes count: " << originalNodesCount << std::endl;
		file << "all supernodals: " << allSuperNodals << std::endl;
		file << "supernodes count: " << superNodesCount << std::endl;
		file << std::endl << "-- end of one run --" << std::endl;
		file.close();
	}

	~DataCollector() {}
};
