#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <map>
#include <omp.h>

#include "MainMethods.h"

CSCMatrix loadMatrix(std::string fileName) {
	std::ifstream file(fileName + ".txt");
	if (!file) {
		throw std::runtime_error("file was not opened, try again");
	}

	int counter = 0;
	CSCMatrix data = CSCMatrix();
	data.getColumns().push_back(counter);

	int tempr = 0;
	std::string line;
	while (std::getline(file, line)) {
		int r, c;
		double v;

		std::istringstream iss(line);
		if (!(iss >> r >> c >> v)) { 
			throw std::runtime_error("Wrong input data format"); 
		}
		if (r != tempr) { 
			data.getColumns().push_back(counter);
		}
		tempr = r;
		data.getRows().push_back(c);
		data.getValues().push_back(v);

		counter++;
	}
	data.getColumns().push_back(counter);

	file.close();
	return data;
}

std::vector<double> loadRightSide(std::string fileName) {
	std::ifstream file(fileName + ".txt");
	if (!file) {
		throw std::runtime_error("file was not opened, try again");
	}

	double v;
	std::vector<double> data = std::vector<double>();
	std::string line;
	while (std::getline(file, line))
	{
		std::istringstream iss(line);
		if (!(iss >> v)) {
			throw std::runtime_error("Wrong input data format");
		}
		data.push_back(v);
	}

	file.close();
	return data;
}

void exportResult(std::string fileName, std::vector<double>& result) {
	std::ofstream file;
	file.open(fileName + "_result.txt");

	for (int i = 0; i < result.size(); i++) {
		file << result[i] << std::endl;
	}

	file.close();
}

std::vector<TreeNode*> createEliminationTree(CSCMatrix& AMatrix) {
	std::vector<TreeNode*> nodes = std::vector<TreeNode*>();
	int nodesCount = AMatrix.getColumns().size() - 1;

	for (int i = 0; i < nodesCount; i++) {
		nodes.push_back(new TreeNode(i));
		nodes[i]->setAdjacents(getAdjacents(AMatrix, i));
	}

	std::vector<int> nonZeros;
	for (int i = 0; i < nodesCount; i++) {
		nonZeros = getRowElements(AMatrix, i);

		for (int j = 0; j < nonZeros.size(); j++) {
			int node = nonZeros[j];
			while (nodes[node]->getParent() != NULL) {
				node = nodes[node]->getParent()->getIndeces()[0];
			}
			if (node != i) {
				nodes[node]->setParent(nodes[i]);
				nodes[i]->getChildren().push_back(nodes[node]);
				nodes[i]->joinAdjacents(nodes[node]->getAdjacents());
			}
		}
		nonZeros.clear();
	}
	// if you don't use supernodal method, return leaves after this section of code
	/*std::vector<TreeNode>* leaves = new std::vector<TreeNode*>();
	for (int i = 0; i < nodesCount; i++) {
		if (nodes[i]->isLeaf()) {
			leaves->push_back(nodes[i]);
		}
	}*/

	return nodes;
}

std::vector<TreeNode*> transformToSuperNodes(std::vector<TreeNode*>& nodes, DataCollector& dc) {
	int nodesCount = nodes.size();
	int allSupernodals = 0;
	std::vector<int> nodesToErase = std::vector<int>();
	std::vector<TreeNode*> leaves = std::vector<TreeNode*>();
	int counter = 0;

	for (int i = 0; i < nodesCount; i++) {
		if (nodes[i]->isLeaf()) {
			leaves.push_back(nodes[i]);
		}

		std::set<int> indeces = std::set<int>();
		int jump = 0;

		TreeNode* mightBeSuperNode = nodes[i];
		for (int j = i + 1; j < nodesCount; j++) {
			indeces.insert(j);
			std::set<int> current;
			current.insert(indeces.begin(), indeces.end());
			current.insert(nodes[j]->getAdjacents()->begin(), nodes[j]->getAdjacents()->end());

			if (*mightBeSuperNode->getAdjacents() == current) {
				nodesToErase.push_back(j);
				mightBeSuperNode->getIndeces().push_back(j);
				jump++;
				std::vector<TreeNode*> children = nodes[j]->getChildren();
				for (int k = 0; k < children.size(); k++) {
					if (children[k] == nodes[j - 1]) {
						children.erase(children.begin() + k);
						break;
					}
				}
				mightBeSuperNode->getChildren().insert(mightBeSuperNode->getChildren().end(), children.begin(), children.end());
				if (j == nodesCount - 1) {
					counter++;
					allSupernodals += mightBeSuperNode->getIndeces().size();
					nodes[i]->setParent(NULL);
					for (int k = 0; k < mightBeSuperNode->getChildren().size(); k++) {
						mightBeSuperNode->getChildren()[k]->setParent(mightBeSuperNode);
					}
				}
			}
			else if (jump > 0) {
				counter++;
				allSupernodals += mightBeSuperNode->getIndeces().size();
				TreeNode* parent = nodes[j - 1]->getParent();
				mightBeSuperNode->setParent(parent);

				if (parent != NULL) {
					for (int k = 0; k < parent->getChildren().size(); k++) {
						if (parent->getChildren()[k] == nodes[j - 1]) {
							parent->getChildren().erase(parent->getChildren().begin() + k);
							break;
						}
					}
					parent->getChildren().push_back(mightBeSuperNode);
					for (int k = 0; k < mightBeSuperNode->getChildren().size(); k++) {
						mightBeSuperNode->getChildren()[k]->setParent(mightBeSuperNode);
					}
				}
				break;
			} 
			else {
				break;
			}
		}
		nodes[i]->clearAdjacents();
		i += jump;
	}
	safelyRemoveNodes(nodes, nodesToErase);
	dc.setNodes(nodesCount, allSupernodals, counter);
	std::cout << " (" << counter << " supernode/s) ";

	return leaves;
}

void safelyRemoveNodes(std::vector<TreeNode*>& nodes, std::vector<int>& indeces) {
	for (int i = 0; i < indeces.size(); i++) {
		int index = indeces[i];
		nodes[index]->clearAdjacents();
		nodes[index]->getChildren().clear();
		delete nodes[index];
	}
}

std::vector<int> getRowElements(CSCMatrix& matrix, int index) {
	int columnIndex;
	int startIndex = matrix.getColumns().at(index);
	int endIndex = matrix.getColumns().at(index + 1);

	std::vector<int> result = std::vector<int>();
	for (int i = startIndex; i < endIndex; i++) {
		columnIndex = matrix.getRows().at(i);
		if (columnIndex < index) {
			result.push_back(columnIndex);
		}
		else {
			return result;
		}
	}

	return result;
}

std::set<int>* getAdjacents(CSCMatrix& matrix, int index) {
	int startIndex = matrix.getColumns().at(index);
	int endIndex = matrix.getColumns().at(index + 1);
	std::set<int>* adjacents = new std::set<int>();

	for (int i = startIndex; i < endIndex; i++) {
		if (matrix.getRows().at(i) > index) {
			adjacents->insert(matrix.getRows().at(i));
		}
	}

	return adjacents;
}

CSCMatrix performFactorization(std::vector<TreeNode*>& leaves, CSCMatrix& AMatrix, std::vector<TreeNode*>& roots) {
	CSCMatrix result = CSCMatrix();
	result.setColumns(std::vector<int>(AMatrix.getColumns().size(), 0));

	#pragma omp parallel for
		for (int i = 0; i < leaves.size(); i++) {
			leaves[i]->performElimination(result, AMatrix, roots);
		}
	
	return result;
}

std::vector<double> performForwardSubstitution(std::vector<TreeNode*>& leaves, CSCMatrix& LMatrix, std::vector<double>& rightSide) {
	std::vector<double> result = std::vector<double>(rightSide.size(), 0.0);

	#pragma omp parallel for
	for (int i = 0; i < leaves.size(); i++) {
		leaves[i]->performForwardSubstitution(result, LMatrix, rightSide);
	}

	return result;
}

std::vector<double> performBackwardSubstitution(std::vector<TreeNode*>& roots, CSCMatrix& LTMatrix, std::vector<double>& rightSide) {
	std::vector<double> result = std::vector<double>(rightSide.size(), 0.0);

	#pragma omp parallel for
	for (int i = 0; i < roots.size(); i++) {
		roots[i]->performBackwardSubstitution(result, LTMatrix, rightSide);
	}

	return result;
}

void clearTree(std::vector<TreeNode*>& roots) {
	for (int i = 0; i < roots.size(); i++) {
		delete roots[i];
	}
}
