#pragma once

#include "TreeNode.h"
#include "Vector.h"
#include "DataCollector.h"

// loads input matrix in COO format and converts it into CSC format
CSCMatrix loadMatrix(std::string fileName);

// loads right side of the equation
std::vector<double> loadRightSide(std::string fileName);

// exports result into txt file
void exportResult(std::string fileName, std::vector<double>& result);

// creates elimination tree for given matrix and returns all leaves
std::vector<TreeNode*> createEliminationTree(CSCMatrix& AMatrix);

// transforms elimination tree to supernodal elimination tree and returns all leaves
std::vector<TreeNode*> transformToSuperNodes(std::vector<TreeNode*>& nodes, DataCollector& dc);

// removes unused nodes that were merged into one super node
void safelyRemoveNodes(std::vector<TreeNode*>& nodes, std::vector<int>& indeces);

// returns column indeces of nonzero elements in given row (returns only elements with i < j indeces)
std::vector<int> getRowElements(CSCMatrix& matrix, int index);

// returns indexes of adjacent nodes for given node (given by its index)
std::set<int>* getAdjacents(CSCMatrix& matrix, int index);

// starts multifrontal method on given elimination tree (its leaves) and input matrix
CSCMatrix performFactorization(std::vector<TreeNode*>& leaves, CSCMatrix& AMatrix, std::vector<TreeNode*>& roots);

// performs forward part of the solve phase
std::vector<double> performForwardSubstitution(std::vector<TreeNode*>& leaves, CSCMatrix& LMatrix, std::vector<double>& rightSide);

// performs backward part of the solve phase
std::vector<double> performBackwardSubstitution(std::vector<TreeNode*>& roots, CSCMatrix& LTMatrix, std::vector<double>& rightSide);

// clears memory 
void clearTree(std::vector<TreeNode*>& roots);
