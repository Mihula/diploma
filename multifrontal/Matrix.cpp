#include <iostream>

#include "Matrix.h"

void Matrix::prepareValues() {
	int size = indeces.size();
	for (int i = 0; i < size; i++) {
		values.push_back(std::vector<double>(size, 0));
	}
}

void Matrix::printMatrix() {
	int size = values.size();

	std::cout << "Indexes: ";
	for (int i = 0; i < size; i++) {
		std::cout << indeces[i] << " ";
	}
	std::cout << std::endl;

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			std::cout << values[i][j] << " ";
		}
		std::cout << std::endl;
	}
}