#pragma once

#include <vector>

class Matrix {

private:
	std::vector<int> indeces = std::vector<int>();
	std::vector<std::vector<double>> values = std::vector<std::vector<double>>();

public:
	Matrix() {}

	Matrix(std::vector<int> indeces, std::vector<std::vector<double>> values) {
		this->indeces = indeces;
		this->values = values;
	}

	void prepareValues();
	void printMatrix();

	std::vector<int>& getIndeces() { return indeces; }
	std::vector<std::vector<double>>& getValues() { return values; }

	void setIndeces(std::vector<int> indeces) {
		this->indeces = indeces;
	}

	~Matrix() {}
};
