#include <iostream>
#include <omp.h>
#include <map>
#include <cmath>

#include "TreeNode.h"

void TreeNode::performElimination(CSCMatrix& resultMatrix, CSCMatrix& inputMatrix, std::vector<TreeNode*>& roots) {
	for (int i = 0; i < children.size(); i++) {
		if (!children[i]->isReady()) {
			return;
		}
	}
	
	std::vector<Matrix> matrixes = std::vector<Matrix>();
	Matrix aMatrix = getAMatrix(inputMatrix);
	matrixes.push_back(aMatrix);
	for (int i = 0; i < children.size(); i++) {
		matrixes.push_back(*children.at(i)->getUpdateMatrix());
	}

	Matrix frontalMatrix = extendAddMatrixes(matrixes);
	for (int i = 0; i < children.size(); i++) {
		children[i]->clearUpdateMatrix();
	}

	calculateColumns(frontalMatrix);

	#pragma omp critical 
	{
		resultMatrix.insertResults(frontalMatrix, indeces);
	}

	if (parent != NULL) {
		updateMatrix = performEliminationStep(frontalMatrix);
		ready = true;
		parent->performElimination(resultMatrix, inputMatrix, roots);
	} else {
		roots.push_back(this);
	}
}

void TreeNode::performForwardSubstitution(std::vector<double>& result, CSCMatrix& LMatrix, std::vector<double>& rightSide) {
	for (int i = 0; i < children.size(); i++) {
		if (!children[i]->isSubstitutionReady()) {
			return;
		}
	}

	int n = indeces.size();
	for (int i = 0; i < n; i++) {
		int index = indeces[i];
		int start = LMatrix.getColumns()[index];
		int end = LMatrix.getColumns()[index + 1];

		int firstIndex = LMatrix.getRows()[start];
		result[firstIndex] += rightSide[firstIndex];
		result[firstIndex] /= LMatrix.getValues()[start];

		for (int j = start + 1; j < end; j++) {
			result[LMatrix.getRows()[j]] -= result[firstIndex]*LMatrix.getValues()[j];
		}
	}

	if (parent != NULL) {
		ready = false;
		parent->performForwardSubstitution(result, LMatrix, rightSide);
	}
}

void TreeNode::performBackwardSubstitution(std::vector<double>& result, CSCMatrix& LTMatrix, std::vector<double>& rightSide) {
	int n = indeces.size() - 1;

	for (int i = n; i >= 0; i--) {
		int index = indeces[i];
		int start = LTMatrix.getColumns()[index + 1] - 1;
		int end = LTMatrix.getColumns()[index];

		int firstIndex = LTMatrix.getRows()[start];
		result[firstIndex] += rightSide[firstIndex];
		result[firstIndex] /= LTMatrix.getValues()[start];

		for (int j = start - 1; j >= end; j--) {
			result[LTMatrix.getRows()[j]] -= result[firstIndex]*LTMatrix.getValues()[j];
		}
	}
	ready = true;

	for (int i = 0; i < children.size(); i++) {
		children[i]->performBackwardSubstitution(result, LTMatrix, rightSide);
	}
}

void TreeNode::printAdjacents() {
	for (int i : *adjacents) {
		std::cout << i << ' ';
	}
	std::cout << std::endl;
}

void TreeNode::clearAdjacents() {
	adjacents->clear();
	delete adjacents;
}

void TreeNode::clearUpdateMatrix() {
	delete updateMatrix;
}

void TreeNode::joinAdjacents(std::set<int>* insertedSet) {
	adjacents->insert(insertedSet->begin(), insertedSet->end());
	adjacents->erase(this->indeces[0]);
}

bool TreeNode::isRoot() {
	return parent == NULL;
}

bool TreeNode::isLeaf() {
	return children.empty();
}

Matrix TreeNode::getAMatrix(CSCMatrix& matrix) {
	std::vector<Vector> vectors = std::vector<Vector>();

	for (int i = 0; i < indeces.size(); i++) {
		vectors.push_back(getAVectorByIndex(matrix, indeces[i]));
	}
	Matrix result = extendAddVectors(vectors);

	return result;
}

Matrix TreeNode::extendAddMatrixes(std::vector<Matrix>& matrixes) {
	std::vector<int> finalIndeces = unionIndeces(matrixes);

	std::map<int, int> indexRedirection;
	for (int i = 0; i < finalIndeces.size(); i++) {
		indexRedirection[finalIndeces.at(i)] = i;
	}

	Matrix result = getEmptyMatrix(finalIndeces);

	for (int i = 0; i < matrixes.size(); i++) {
		Matrix currentMatrix = matrixes[i];
		int size = currentMatrix.getIndeces().size();

		for (int j = 0; j < size; j++) {
			for (int k = 0; k < size; k++) {
				int targetRowIndex = indexRedirection[currentMatrix.getIndeces()[j]];
				int targetColumnIndex = indexRedirection[currentMatrix.getIndeces()[k]];
				result.getValues()[targetRowIndex][targetColumnIndex] += currentMatrix.getValues()[j][k];
			}
		}
	}

	return result;
}

void TreeNode::calculateColumns(Matrix& frontalMatrix) {
	int indecesSize = indeces.size();

	for (int i = 0; i < indecesSize; i++) {
		double diagonalRoot = sqrt(frontalMatrix.getValues()[i][i]);
		frontalMatrix.getValues()[i][i] = diagonalRoot;

		int size = frontalMatrix.getIndeces().size();
		for (int j = i + 1; j < size; j++) {
			frontalMatrix.getValues()[i][j] /= diagonalRoot;
		}
		if (i == indecesSize - 1) {
			return;
		}

		#pragma omp parallel for
		for (int k = i + 1; k < size; k++) {
			for (int l = i + 1; l < size; l++) {
				double multiplication = frontalMatrix.getValues()[i][k] * frontalMatrix.getValues()[i][l];
				frontalMatrix.getValues()[k][l] -= multiplication;
			}
		}
	}
}

Vector TreeNode::getAVectorByIndex(CSCMatrix& matrix, int index) {
	int startIndex = matrix.getColumns()[index];
	int endIndex = matrix.getColumns()[index + 1];
	Vector columnVector = Vector();

	for (int i = startIndex; i < endIndex; i++) {
		int rowIndex = matrix.getRows()[i];

		if (rowIndex >= index) {
			columnVector.getIndeces().push_back(rowIndex);
			columnVector.getValues().push_back(matrix.getValues()[i]);
		}
	}

	return columnVector;
}

Matrix TreeNode::extendAddVectors(std::vector<Vector>& vectors) {
	std::vector<int> finalIndeces = unionIndeces(vectors);

	std::map<int, int> indexRedirection;
	for (int i = 0; i < finalIndeces.size(); i++) {
		indexRedirection[finalIndeces[i]] = i;
	}

	Matrix result = getEmptyMatrix(finalIndeces);

	for (int i = 0; i < vectors.size(); i++) {
		Vector currentVector = vectors[i];
		int size = currentVector.getIndeces().size();

		for (int j = 0; j < size; j++) {
			int targetIndex = indexRedirection[currentVector.getIndeces()[j]];
			result.getValues()[i][targetIndex] = currentVector.getValues()[j];
			result.getValues()[targetIndex][i] = currentVector.getValues()[j];
		}
	}

	return result;
}

std::vector<int> TreeNode::unionIndeces(std::vector<Matrix>& matrixes) {
	std::set<int> all;

	for (int i = 0; i < matrixes.size(); i++) {
		std::vector<int> indeces = matrixes[i].getIndeces();
		all.insert(indeces.begin(), indeces.end());
	}

	return std::vector<int>(all.begin(), all.end());
}

std::vector<int> TreeNode::unionIndeces(std::vector<Vector>& vectors) {
	std::set<int> all;

	for (int i = 0; i < vectors.size(); i++) {
		std::vector<int> indeces = vectors[i].getIndeces();
		all.insert(indeces.begin(), indeces.end());
	}

	return std::vector<int>(all.begin(), all.end());
}

Matrix TreeNode::getEmptyMatrix(std::vector<int>& indeces) {
	Matrix emptyMatrix = Matrix();
	emptyMatrix.setIndeces(std::vector<int>(indeces));
	emptyMatrix.prepareValues();
	
	return emptyMatrix;
}

Matrix TreeNode::getUpdateMatrix(Matrix& frontalMatrix, int superNodeElements) {
	std::vector<int> indeces = std::vector<int>(frontalMatrix.getIndeces().begin() + superNodeElements, frontalMatrix.getIndeces().end());
	Matrix result = getEmptyMatrix(indeces);

	#pragma omp parallel for
	for (int i = 0; i < indeces.size(); i++) {
	#pragma omp parallel for
		for (int j = 0; j < indeces.size(); j++) {
			result.getValues()[i][j] = frontalMatrix.getValues()[i + superNodeElements][j + superNodeElements];
		}
	}

	return result;
}

Matrix* TreeNode::performEliminationStep(Matrix& frontalMatrix) {
	int startIndex = indeces.size() - 1;
	int size = frontalMatrix.getIndeces().size();

	#pragma omp parallel for
	for (int k = startIndex + 1; k < size; k++) {
	#pragma omp parallel for
		for (int l = startIndex + 1; l < size; l++) {
			double multiplication = frontalMatrix.getValues()[startIndex][k] * frontalMatrix.getValues()[startIndex][l];
			frontalMatrix.getValues()[k][l]-= multiplication;
		}
	}
	Matrix updateMatrix = getUpdateMatrix(frontalMatrix, startIndex + 1);

	return new Matrix(updateMatrix.getIndeces(), updateMatrix.getValues());
}