#pragma once

#include <set>
#include "Matrix.h"
#include "CSCMatrix.h"
#include "Vector.h"

// represents node in tree structure
class TreeNode {

private:
	std::vector<int> indeces = std::vector<int>();
	bool ready = false;
	std::vector<TreeNode*> children = std::vector<TreeNode*>();
	std::set<int>* adjacents = new std::set<int>();
	TreeNode* parent = NULL;
	Matrix* updateMatrix = NULL;
	
	// creates matrix from CSC matrix - used for representation of "original" matrix in creation of frontal matrix
	Matrix getAMatrix(CSCMatrix& matrix);
	
	// extends adds given matrixes
	Matrix extendAddMatrixes(std::vector<Matrix>& matrixes);
	
	// calculates "eliminated" columns (more at one time if given node is supernode)
	void calculateColumns(Matrix& frontalMatrix);
	
	// creates vector from CSC matrix by given index - used for creation of "original" matrix in creation of frontal matrix
	Vector getAVectorByIndex(CSCMatrix& matrix, int index);

	// extends adds given vectors - used for creation of "original" matrix in creation of super frontal matrix
	Matrix extendAddVectors(std::vector<Vector>& vectors);
	
	// creates vector of original indeces for result matrix in extend add operation
	std::vector<int> unionIndeces(std::vector<Matrix>& matrixes);

	// creates vector of original indeces for result matrix in extend add operation (supernodal version)
	std::vector<int> unionIndeces(std::vector<Vector>& vectors);
	
	// creates matrix with elements equals to zero from given vector of original indeces
	Matrix getEmptyMatrix(std::vector<int>& indeces);
	
	// returns update matrix from computed frontal matrix
	Matrix getUpdateMatrix(Matrix& frontalMatrix, int superNodeElements);

	// does elimination step on given frontal matrix and returns update matrix for given node
	Matrix* performEliminationStep(Matrix& frontalMatrix);

public:
	TreeNode() {}
	TreeNode(int index) { indeces.push_back(index); }

	void performElimination(CSCMatrix& resultMatrix, CSCMatrix& inputMatrix, std::vector<TreeNode*>& roots);
	void performBackwardSubstitution(std::vector<double>& result, CSCMatrix& LTMatrix, std::vector<double>& rightSide);
	void performForwardSubstitution(std::vector<double>& result, CSCMatrix& LMatrix, std::vector<double>& rightSide);
	void printAdjacents();
	void clearAdjacents();
	void clearUpdateMatrix();
	void joinAdjacents(std::set<int>* insertedSet);

	bool isRoot();
	bool isLeaf();
	bool isReady() { return ready; };
	bool isSubstitutionReady() { return !ready; };

	void setParent(TreeNode* parent) { this->parent = parent; }
	void setAdjacents(std::set<int>* adjacents) { this->adjacents = adjacents; }

	std::vector<int>& getIndeces() { return indeces; }
	Matrix* getUpdateMatrix() { return updateMatrix; }
	TreeNode* getParent() { return parent; }
	std::vector<TreeNode*>& getChildren() { return children; }
	std::set<int>* getAdjacents() { return adjacents; }

	~TreeNode() {
		for (int i = 0; i < children.size(); i++) {
			delete children[i];
		}
	}
};
