#pragma once

#include <vector>

class Vector {

private:
	std::vector<int> indeces = std::vector<int>();
	std::vector<double> values = std::vector<double>();

public:
	Vector() {}

	void printVector();

	void setIndeces(std::vector<int> indeces) {
		this->indeces = indeces;
	}

	std::vector<int>& getIndeces() { return indeces; }
	std::vector<double>& getValues() { return values; }

	~Vector() {}
};