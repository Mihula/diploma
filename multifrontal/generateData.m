function [ A, b ] = generateData(size)

density = 0.05;

A = sprandsym(size, density, 0.1, 1);
b = rand(size, 1);

[i,j,val] = find(A);
for index=1:length(i)
    i(index) = i(index)-1;
    j(index) = j(index)-1;
end
unordered = [i'; j'; val']';
result = sortrows(unordered,1);

fid = fopen('A.txt','w');
fprintf(fid,'%d %d %f\n', transpose(result));
fclose(fid);
fid = fopen('b.txt','w');
fprintf(fid,'%f \n', transpose(b));
fclose(fid);

end