#include <iostream>

#include "MainMethods.h"
#include "DataCollector.h"

int main() {
	std::cout << "Enter input matrix file name: ";
	std::string inputFile;
	CSCMatrix AMatrix;
	int ready = false;
	while (std::getline(std::cin, inputFile) && !inputFile.empty()) {
		try {
			AMatrix = loadMatrix(inputFile);
			std::cout << "File loaded, press enter to continue";
			ready = true;
		}
		catch (std::runtime_error re) {
			std::cout << "Runtime error: " << re.what() << std::endl;
			continue;
		}
	}

	if (!ready) {
		return 1;
	}

	DataCollector dc = DataCollector();
	dc.start("creating elimination tree");
	std::vector<TreeNode*> treeNodes = createEliminationTree(AMatrix);
	dc.split("transforming to supernodes");
	std::vector<TreeNode*> leaves = transformToSuperNodes(treeNodes, dc);
	dc.split("factorization");
	std::vector<TreeNode*> roots = std::vector<TreeNode*>();
	CSCMatrix LMatrix = performFactorization(leaves, AMatrix, roots);
	dc.setRootsAndLeaves(roots.size(), leaves.size());
	dc.split("transposing matrix");
	CSCMatrix LTMatrix = LMatrix.transposeMatrix();
	dc.pause();
	
	std::cout << std::endl << "Enter right side file name: ";
	std::string fileName;
	while (std::getline(std::cin, fileName) && !fileName.empty()) {
		std::vector<double> bSide;
		try {
			bSide = loadRightSide(fileName);
		}
		catch (std::runtime_error re) {
			std::cout << "Runtime error: " << re.what() << std::endl;
			std::cout << "Enter another right side file name (press only enter to finish): ";
			continue;
		}

		dc.start("forward substitution");
		std::vector<double> ySide = performForwardSubstitution(leaves, LMatrix, bSide);
		dc.split("backward substitution");
		std::vector<double> result = performBackwardSubstitution(roots, LTMatrix, ySide);
		dc.pause();
		exportResult(fileName, result);
		std::cout << std::endl << "Enter another right side file name (press only enter to finish): ";
	}
	dc.stop();
	dc.exportData("statistics.txt");
	clearTree(roots);

	std::cin.get();
	return 0;
}